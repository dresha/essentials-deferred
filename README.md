```javascript
    var rendering = mmcore.Deferred();
    rendering.done(function(){
        console.log('Finished rendering');
    });

    //...

    rendering.resolve();
```

Simply Deferred is fullly compatible with jQuery's API, so the [docs and usage](http://api.jquery.com/category/deferred-object/) are the same. Like the jQuery deferred API, it provides the following methods:

* `Deferred()`
* `deferred.state()`
* `deferred.done()`
* `deferred.fail()`
* `deferred.progress()`
* `deferred.always()`
* `deferred.promise()`
* `deferred.notify()`
* `deferred.notifyWith()`
* `deferred.resolve()`
* `deferred.resolveWith()`
* `deferred.rejectWith()`
* `deferred.reject()`
* `deferred.pipe()`
* `deferred.then()`
* `Deferred.when()`
