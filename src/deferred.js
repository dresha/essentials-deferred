/**
 * Essentials - Deferred
 *
 * A handy tool for organizing waiting for something.
 *
 * @version 0.0.3
 *
 *
 * @author a.porohnya@gmail.com (Aleksey Porokhnya)
 */
;(function() {
    var Deferred, PENDING, REJECTED, RESOLVED, after, execute, flatten, has, isArguments, isPromise, wrap, _when,
        __slice = [].slice;

    PENDING = "pending";

    RESOLVED = "resolved";

    REJECTED = "rejected";

    if ('function' !== typeof Array.prototype.reduce) {
        Array.prototype.reduce = function(callback /*, initialValue*/) {
            if (null === this || 'undefined' === typeof this) {
                throw new TypeError('Array.prototype.reduce called on null or undefined');
            }
            if ('function' !== typeof callback) {
                throw new TypeError(callback + ' is not a function');
            }
            var t = Object(this), len = t.length >>> 0, k = 0, value;
            if (arguments.length >= 2) {
                value = arguments[1];
            } else {
                while (k < len && ! k in t) k++;
                if (k >= len)
                    throw new TypeError('Reduce of empty array with no initial value');
                value = t[k++];
            }
            for (; k < len; k++) {
                if (k in t) {
                    value = callback(value, t[k], k, t);
                }
            }
            return value;
        };
    }

    if (!Array.isArray) {
        Array.isArray = function(arg) {
            return Object.prototype.toString.call(arg) === '[object Array]';
        };
    }

    has = function(obj, prop) {
        return obj != null ? obj.hasOwnProperty(prop) : void 0;
    };

    isArguments = function(obj) {
        return has(obj, 'length') && has(obj, 'callee');
    };

    isPromise = function(obj) {
        return has(obj, 'promise') && typeof (obj != null ? obj.promise : void 0) === 'function';
    };

    flatten = function(array) {
        if (isArguments(array)) {
            return flatten(Array.prototype.slice.call(array));
        }
        if (!Array.isArray(array)) {
            return [array];
        }
        return array.reduce(function(memo, value) {
            if (Array.isArray(value)) {
                return memo.concat(flatten(value));
            }
            memo.push(value);
            return memo;
        }, []);
    };

    after = function(times, func) {
        if (times <= 0) {
            return func();
        }
        return function() {
            if (--times < 1) {
                return func.apply(this, arguments);
            }
        };
    };

    wrap = function(func, wrapper) {
        return function() {
            var args;
            args = [func].concat(Array.prototype.slice.call(arguments, 0));
            return wrapper.apply(this, args);
        };
    };

    execute = function(callbacks, args, context) {
        var callback, _i, _len, _ref, _results;
        _ref = flatten(callbacks);
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            callback = _ref[_i];
            if (Object.prototype.toString.call(args) !== '[object Arguments]' && args.length === undefined) {
                args.length = 0;
            }
            _results.push(callback.call.apply(callback, [context].concat(__slice.call(args))));
        }
        return _results;
    };

    /**
     * @name Deferred
     * @namespace
     * @returns {Deferred}
     * @constructor
     */
    Deferred = function() {
        var candidate, close, closingArguments, doneCallbacks, failCallbacks, progressCallbacks, state;
        state = PENDING;
        doneCallbacks = [];
        failCallbacks = [];
        progressCallbacks = [];
        closingArguments = {
            'resolved': {},
            'rejected': {},
            'pending': {}
        };
        /**
         * Promise Object
         *
         * @type {Promise}
         * @param candidate
         * @returns {{}}
         */
        this.promise = function(candidate) {
            var pipe, storeCallbacks;
            candidate = candidate || {};
            candidate.state = function() {
                return state;
            };
            storeCallbacks = function(shouldExecuteImmediately, holder, holderState) {
                return function() {
                    if (state === PENDING) {
                        holder.push.apply(holder, flatten(arguments));
                    }
                    if (shouldExecuteImmediately()) {
                        execute(arguments, closingArguments[holderState]);
                    }
                    return candidate;
                };
            };
            candidate.done = storeCallbacks((function() {
                return state === RESOLVED;
            }), doneCallbacks, RESOLVED);
            candidate.fail = storeCallbacks((function() {
                return state === REJECTED;
            }), failCallbacks, REJECTED);
            candidate.progress = storeCallbacks((function() {
                return state !== PENDING;
            }), progressCallbacks, PENDING);
            candidate.always = function() {
                var _ref;
                return (_ref = candidate.done.apply(candidate, arguments)).fail.apply(_ref, arguments);
            };
            pipe = function(doneFilter, failFilter, progressFilter) {
                var filter, master;
                master = new Deferred();
                filter = function(source, funnel, callback) {
                    if (!callback) {
                        return candidate[source](master[funnel]);
                    }
                    return candidate[source](function() {
                        var args, value;
                        args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
                        value = callback.apply(null, args);
                        if (isPromise(value)) {
                            return value.done(master.resolve).fail(master.reject).progress(master.notify);
                        } else {
                            return master[funnel](value);
                        }
                    });
                };
                filter('done', 'resolve', doneFilter);
                filter('fail', 'reject', failFilter);
                filter('progress', 'notify', progressFilter);
                return master;
            };
            candidate.pipe = pipe;
            candidate.then = pipe;
            if (candidate.promise == null) {
                candidate.promise = function() {
                    return candidate;
                };
            }
            return candidate;
        };
        this.promise(this);
        candidate = this;
        close = function(finalState, callbacks, context) {
            return function() {
                if (state === PENDING) {
                    state = finalState;
                    closingArguments[finalState] = arguments;
                    execute(callbacks, closingArguments[finalState], context);
                    return candidate;
                }
                return this;
            };
        };
        this.resolve = close(RESOLVED, doneCallbacks);
        this.reject = close(REJECTED, failCallbacks);
        this.notify = close(PENDING, progressCallbacks);
        this.resolveWith = function(context, args) {
            return close(RESOLVED, doneCallbacks, context).apply(null, args);
        };
        this.rejectWith = function(context, args) {
            return close(REJECTED, failCallbacks, context).apply(null, args);
        };
        this.notifyWith = function(context, args) {
            return close(PENDING, progressCallbacks, context).apply(null, args);
        };
        return this;
    };

    /**
     * Provides a way to execute callback functions based on one or more objects, usually Deferred objects that represent asynchronous events.
     *
     * @returns {Promise}
     * @private
     */
    _when = function() {
        var def, defs, finish, resolutionArgs, trigger, _i, _len;
        defs = flatten(arguments);
        if (defs.length === 1) {
            if (isPromise(defs[0])) {
                return defs[0];
            } else {
                return (new Deferred()).resolve(defs[0]).promise();
            }
        }
        trigger = new Deferred();
        if (!defs.length) {
            return trigger.resolve().promise();
        }
        resolutionArgs = [];
        finish = after(defs.length, function() {
            return trigger.resolve.apply(trigger, resolutionArgs);
        });
        defs.forEach(function(def, index) {
            if (isPromise(def)) {
                return def.done(function() {
                    var args;
                    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
                    resolutionArgs[index] = args.length > 1 ? args : args[0];
                    return finish();
                });
            } else {
                resolutionArgs[index] = def;
                return finish();
            }
        });
        for (_i = 0, _len = defs.length; _i < _len; _i++) {
            def = defs[_i];
            isPromise(def) && def.fail(trigger.reject);
        }
        return trigger.promise();
    };


    /**
     * Returns new Deferred object
     *
     * @returns {Deferred}
     * @constructor
     */
    mmcore.Deferred = function () {
        return new Deferred();
    };
    mmcore.when = _when;

}).call(this);